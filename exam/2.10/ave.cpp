#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int main()
{
    int n;
    vector<int> x;
    cin >> n;
    double all = 0;
    for (int i = 0; i < n; i++)
    {
        int num;
        cin >> num;
        all += num;
        x.push_back(num);
    }
    double u = all / n;
    for (int i = 0, all = 0; i < n; i++)
    {
        all += (pow(x[i], 2) - pow(u, 2));
    }
    double ans = all / n;
    cout << "answer : " << ans << endl;
}