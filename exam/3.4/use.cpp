#include "IntGenerator.h"
#include <iostream>
using namespace std;

int main(void)
{
    IntGenerator a(1);
    for (int i = 1; i <= 100; i++)
    {
        a.an = (12345 * a.next(a.an) + 17) % 100;
        cout
            << "a(" << i << ")"
            << " : " << a.an << endl;
    }
}