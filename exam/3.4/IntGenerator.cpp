#include "IntGenerator.h"

IntGenerator ::IntGenerator(int a)
{
  this->an = a;
}

int IntGenerator::next(int a)
{
  int old = this->an;
  this->an = a;
  return old;
}