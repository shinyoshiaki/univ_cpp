#include "Accumulator.h"

Accumulator::Accumulator()
{
  this->sum = 0.0;
  this->num = 0;
}

void Accumulator::addNewData(double add)
{
  this->sum += add;
  this->num++;
}

double Accumulator::getAverage()
{
  return this->sum / this->num;
}