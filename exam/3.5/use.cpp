#include "Accumulator.h"
#include <iostream>
using namespace std;

int main(void)
{
    Accumulator acc;
    for (int i = 1; i <= 10; i++)
    {
        acc.addNewData(i);
    }
    cout << acc.getAverage() / 10 << endl;
}