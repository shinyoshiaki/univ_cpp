#ifndef __Class_Accumulator
#define __Class_Accumulator

class Accumulator
{
    double sum;
    int num;

  public:
    Accumulator();
    void addNewData(double add);
    double getAverage();
};

#endif