#include "Block.h"
#include <iostream>
#define N 1000000
using namespace std;

int main()
{
    Block *blocks[70];
    blocks[0] = new Block(1);
    for (int i = 1; i < 70; i++)
    {
        blocks[i] = new Block(0);
        blocks[i - 1]->setNext(blocks[i]);
        blocks[i]->setPrevious(blocks[i - 1]);
    }
    for (int i = 2; i <= 53; i++)
    {
        blocks[0]->multi(i, 0);
    }
    blocks[69]->print();
    cout << endl;
}