#include <iostream>
#include "Block.h"
#include <cstddef>
using namespace std;

void Block::multi(int num, int up = 0)
{
    this->value = this->value * num + up;

    int right = this->value % 10;
    int left = (this->value - right) / 10;
    // cout << "debug" << this->id << ":" << left << ":" << right << endl;
    this->value = right;
    if (this->next != NULL)
    {
        this->next->multi(num, left);
    }
}

void Block::setValue(int num) { this->value = num; }

void Block::setNext(Block *next) { this->next = next; }

void Block::setPrevious(Block *previous) { this->previous = previous; }

void Block::print()
{
    cout << this->value;
    if (this->previous != NULL)
    {
        this->previous->print();
    }
}