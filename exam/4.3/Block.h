#ifndef __Class_Block
#define __Class_Block
#include <cstddef>

class Block
{
    int value;    
    Block *next, *previous;

  public:
    Block(int value = 0) : value(value){};
    void setValue(int num);
    void multi(int num, int up);
    void print();
    void setNext(Block *next = NULL);
    void setPrevious(Block *previous = NULL);
};

#endif