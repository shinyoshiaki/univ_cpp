#ifndef __Class_SortBtree
#define __Class_SortBtree

#include <cstddef>
#include <string>

struct Btree
{
  const std::string id;
  const int num;
  Btree *left, *right;
  Btree(std::string id = "", int num = 0, Btree *left = NULL,
        Btree *right = NULL)
      : id(id), num(num), left(left), right(right) {}
};

class SortBtree
{
  Btree *root;
  SortBtree():root(NULL){}
};

#endif