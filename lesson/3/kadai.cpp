#include <iostream>
#include <math.h>
using namespace std;
class Complex
{
  private:
    float re;
    float im;

  public:
    Complex() : re(0), im(0) {}
    Complex(float re, float im) : re(re), im(im) {}
    Complex(float re) : re(re), im(0) {}

    float real() const { return re; }
    float imag() const { return im; }

    Complex &operator=(const Complex &z)
    {
        this->re = z.re;
        this->im = z.im;
        return *this;
    }

    Complex operator+() const { return *this; }
    Complex operator-() const { return Complex(-this->re, -this->im); }
    Complex operator+(const Complex z1) const
    {
        Complex tmp;
        tmp.re = re + z1.re;
        tmp.im = im + z1.im;
        return tmp;
    }
    Complex operator-(const Complex z1) const
    {
        Complex tmp;
        tmp.re = re - z1.re;
        tmp.im = im - z1.im;
        return tmp;
    }
    Complex operator*(const Complex z1) const
    {
        Complex tmp;
        tmp.re = re * z1.re - im * z1.im;
        tmp.im = re * z1.im + im * z1.re;
        return tmp;
    }
    Complex operator/(const Complex z1) const
    {
        Complex tmp;
        tmp.re = (re * z1.re + im * z1.im) / (pow(z1.re, 2) + pow(z1.im, 2));
        tmp.im = (z1.re * im - z1.im * re) / (pow(z1.re, 2) + pow(z1.im, 2));
        return tmp;
    }

    Complex &operator+=(const Complex &z)
    {
        this->re += z.re;
        this->im += z.im;
        return *this;
    }

    Complex &operator+=(const float &z)
    {
        this->re += z;
        return *this;
    }

    Complex &operator-=(const Complex &z)
    {
        this->re -= z.re;
        this->im -= z.im;
        return *this;
    }

    Complex &operator-=(const float &r)
    {
        this->re -= r;
        return *this;
    }

    Complex &operator*=(const Complex &z)
    {
        float re_tmp = this->re * z.re - this->im * z.im;
        float im_tmp = this->re * z.im + this->im * z.re;
        this->re = re_tmp;
        this->im = im_tmp;
        return *this;
    }

    Complex &operator*=(const float k)
    {
        this->re *= k;
        this->im *= k;
        return *this;
    }

    Complex &operator/=(const Complex &z)
    {
        float norm_z = z.re * z.re + z.im * z.im;
        float re_tmp = (this->re * z.re + this->im * z.im) / norm_z;
        float im_tmp = (this->im * z.re - this->re * z.im) / norm_z;
        this->re = re_tmp;
        this->im = im_tmp;
        return *this;
    }

    Complex &operator/=(const float &k)
    {
        this->re /= k;
        this->im /= k;
        return *this;
    }
    Complex conjugate()
    {
        return Complex(re, -im);
    }
    double norm()
    {
        return re * re + im * im;
    }
    void output()
    {
        if (im < 0)
            cout << "Output Complex number: " << re << im << "i";
        else
            cout << "Output Complex number: " << re << "+" << im << "i";
    }
    void input()
    {
        cout << "Enter re and iminary parts respectively: ";
        cin >> re;
        cin >> im;
    }
};
int main()
{
    Complex c1, c2, result, conjugate;
    double norm;
    cout << "Enter first complex number:\n";
    c1.input();
    cout << "Enter second complex number:\n";
    c2.input();

    cout << '+' << endl;
    result = c1 + c2;
    result.output();
    cout << endl;

    conjugate = result.conjugate();
    conjugate.output();
    cout << endl
         << endl;

    norm = result.norm();
    cout << norm << endl
         << endl;

    cout << '-' << endl;
    result = c1 - c2;
    result.output();
    cout << endl;

    conjugate = result.conjugate();
    conjugate.output();
    cout << endl;

    norm = result.norm();
    cout << norm << endl
         << endl;

    cout << '*' << endl;
    result = c1 * c2;
    result.output();
    cout << endl;

    conjugate = result.conjugate();
    conjugate.output();
    cout << endl;

    norm = result.norm();
    cout << norm << endl
         << endl;

    cout << '/' << endl;
    result = c1 / c2;
    result.output();
    cout << endl;

    conjugate = result.conjugate();
    conjugate.output();
    cout << endl;

    norm = result.norm();
    cout << norm << endl
         << endl;
}
