#ifndef ___Class_Quadrangle
#define ___Class_Quadrangle
#include "Point2D.h"
#include <string>

class Quadrangle
{
    Point2D p1, p2, p3, p4;

  public:
    Quadrangle(Point2D *_p1 = new Point2D(), Point2D *_p2 = new Point2D(), Point2D *_p3 = new Point2D(), Point2D *_p4 = new Point2D())
    {
        p1 = *_p1;
        p2 = *_p2;
        p3 = *_p3;
        p4 = *_p4;
    }
    Point2D getP1() const { return p1; };
    Point2D getP2() const { return p2; };
    Point2D getP3() const { return p3; };
    Point2D getP4() const { return p4; };
    std::string toString() const;
    void setP(Point2D p1, Point2D p2, Point2D p3, Point2D p4);
    void plus(const double offsetX, const double offsetY);
};

#endif