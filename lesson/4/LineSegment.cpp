#include <sstream>
#include <string>
#include "LineSegment.h"

using namespace std;
string LineSegment::toString() const
{
    ostringstream os;
    os << "(" << p1.getX() << "," << p1.getY() << ")"
       << ","
       << "(" << p2.getX() << "," << p2.getY() << ")";
    return os.str();
}

void LineSegment::setP(Point2D p1, Point2D p2)
{
    this->p1 = p1;
    this->p2 = p2;
}

void LineSegment::plus(const double offsetX, const double offsetY)
{
    this->p1.plus(offsetX, offsetY);
    this->p2.plus(offsetX, offsetY);
}