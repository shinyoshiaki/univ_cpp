#include <iostream>
#include <string>
#include "Point2D.h"
using namespace std;

inline double square(const double x) { return x * x; }

int main()
{
    Point2D p1(0.5), p2(p1), q[11];
    p2.plus(1, 2);

    for (int i = 0; i <= 10; ++i)
    {
        double x = static_cast<double>(i) / 10.0;
        q[i].setXY(x, square(x));
    }

    cout << "p1=" << p1.toString() << endl;
    cout << "p2=" << p2.toString() << endl;

    for (int i = 0; i <= 10; ++i)
    {
        cout << "q[" << i << "]=" << q[i].toString() << endl;
    }
}