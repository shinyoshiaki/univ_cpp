#include <iostream>
#include <string>
#include <math.h>
#include "Quadrangle.h"
#include "Point2D.h"

using namespace std;

int main()
{
    Quadrangle quad;

    Point2D p1(1, 2), p2(3, 4), p3(5, 7), p4(4, 8);

    quad.setP(p1, p2, p3, p4);
    cout << "quad = " << quad.toString() << endl;
    cout << "plus (4,5)" << endl;
    quad.plus(4, 5);
    cout << "moved quad = " << quad.toString() << endl;
}