#include <sstream>
#include <string>
#include "Quadrangle.h"
using namespace std;
string Quadrangle::toString() const
{
    ostringstream os;
    os << "(" << p1.getX() << "," << p1.getY() << ")"
       << ","
       << "(" << p2.getX() << "," << p2.getY() << ")"
       << ","
       << "(" << p3.getX() << "," << p3.getY() << ")"
       << ","
       << "(" << p4.getX() << "," << p4.getY() << ")";
    return os.str();
}

void Quadrangle::setP(Point2D p1, Point2D p2, Point2D p3, Point2D p4)
{
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;
    this->p4 = p4;
}

void Quadrangle::plus(const double offsetX, const double offsetY)
{
    this->p1.plus(offsetX, offsetY);
    this->p2.plus(offsetX, offsetY);
    this->p3.plus(offsetX, offsetY);
    this->p4.plus(offsetX, offsetY);
}