#ifndef ___Class_LineSegment
#define ___Class_LineSegment
#include "Point2D.h"
#include <string>

class LineSegment
{
    Point2D p1, p2;

  public:
    LineSegment(Point2D *_p1 = new Point2D(), Point2D *_p2 = new Point2D())
    {
        p1 = *_p1;
        p2 = *_p2;
    }
    Point2D getP1() const { return p1; };
    Point2D getP2() const { return p2; };
    std::string toString() const;
    void setP(Point2D p1, Point2D p2);
    void plus(const double offsetX, const double offsetY);
};

#endif