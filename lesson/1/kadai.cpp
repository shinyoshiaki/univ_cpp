#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;
int main(void)
{
    int num = 0;
    cout << "自然数を入力してください。" << endl;
    cin >> num;

    int two = pow(num, 2);
    int three = pow(num, 3);

    int origin = to_string(num).length() + 2;
    two = to_string(two).length() + 2;
    three = to_string(three).length() + 2;

    cout << "数,"
         << "2乗,"
         << "3乗," << endl;

    for (int i = 1; i <= num; i++)
    {
        cout << setw(origin) << left << i << "| "
             << setw(two) << left << pow(i, 2) << "| "
             << setw(three) << left << pow(i, 3) << "| " << endl;
    }
}
