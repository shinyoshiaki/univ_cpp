#include <fstream>
#include <iomanip>
using namespace std;
int main(void)
{
    int i = 5;
    double f = 12.7;
    char ch ='w';
    char str[] = "hello";
    ofstream file("no1-4.outdata");
    file << setw(5) << i << endl;
    file << setw(10) << setiosflags(ios::fixed)
         << setprecision(5) << f << endl;
    file << setw(5) << ch << endl;
    file << setw(8) << str << endl;
}