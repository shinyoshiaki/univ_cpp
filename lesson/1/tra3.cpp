#include <iostream>
#include <iomanip>
using namespace std;
int main(void)
{
    int i = 5;
    double f = 12.7;
    char ch ='w';
    char str[] = "hello";
    cout << setw(5) << i << endl;
    cout << setw(10) << setiosflags(ios::fixed)
         << setprecision(5) << f << endl;
    cout << setw(5) << ch << endl;
    cout << setw(8) << str << endl;
}