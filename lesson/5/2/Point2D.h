#ifndef ___Class_Point2D
#define ___Class_Point2D

#include <iostream>
#include <sstream>
#include <string>

template <typename T>

class Point2D
{
  T x;
  T y;

public:
  Point2D(T x = 0, T y = 0) : x(x), y(y){};
  T getX() const { return x; }
  T getY() const { return y; }
  std::string toString() const;
  void setXY(T x, T y);
  void plus(const Point2D &pt);
  void plus(const T offsetX, const T offsetY);
};

template <typename T>
std::string Point2D<T>::toString() const
{
  std::ostringstream os;
  os << "(" << x << "," << y << ")";
  return os.str();
}

template <typename T>
void Point2D<T>::setXY(T x, T y)
{
  this->x = x;
  this->y = y;
}

template <typename T>
void Point2D<T>::plus(const Point2D &pt)
{
  x += pt.x;
  y += pt.y;
}

template <typename T>
void Point2D<T>::plus(const T offsetX, const T offsetY)
{
  x += offsetX;
  y += offsetY;
}

#endif