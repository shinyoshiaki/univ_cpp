#ifndef ___Class_Point2D
#define ___Class_Point2D

#include <string>

class Point2D
{
    double x;
    double y;

  public:
    Point2D(double x = 0.0, double y = 0.0) : x(x), y(y) {}
    double getX() const { return x; }
    double getY() const { return y; }
    std::string toString() const;
    void setXY(double x = 0.0, double y = 0.0);
    void plus(const Point2D &pt);
    void plus(const double offsetX = 0.0, const double offsetY = 0.0);
};

#endif