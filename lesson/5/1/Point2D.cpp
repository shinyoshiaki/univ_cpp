#include <sstream>
#include <string>
#include "Point2D.h"
using namespace std;
string Point2D::toString() const
{
    ostringstream os;
    os << "(" << x << "," << y << ")";
    return os.str();
}

void Point2D::setXY(double x, double y)
{
    this->x = x;
    this->y = y;
}

void Point2D::plus(const Point2D &pt)
{
    x += pt.x;
    y += pt.y;
}

void Point2D::plus(const double offsetX, const double offsetY)
{
    x += offsetX;
    y += offsetY;
}