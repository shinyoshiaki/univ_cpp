//g++ useLineSegment.cpp LineSegment.cpp Point2D.cpp
#include <iostream>
#include <string>
#include <math.h>
#include "LineSegment.h"
#include "Point2D.h"

using namespace std;

int main()
{
    LineSegment line;

    Point2D p1(1, 2), p2(3, 4);

    line.setP(p1, p2);
    cout << "line = " << line.toString() << endl;
    cout << "plus (4,5)"  << endl;
    line.plus(4, 5);
    cout << "moved line = " << line.toString() << endl;
}