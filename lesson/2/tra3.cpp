#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
    vector<int> vint;
    vector<string> vstr;

    vint.push_back(1);
    vint.push_back(2);
    vint.push_back(3);

    vstr.push_back("123");
    vstr.push_back("456");

    for (int i = 0; i < vint.size(); i++)
    {
        cout << "[" << i << "]=" << vint[i] << endl;
    }

    for (int i = 0; i < vstr.size(); i++)
    {
        cout << "[" << i << "]=" << vstr[i] << endl;
    }
}