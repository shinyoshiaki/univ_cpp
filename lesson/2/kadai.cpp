#include <iostream>
#include <iomanip>

using namespace std;
int main()
{
    string first, second;
    cout << "文字列を2つ入力してください" << endl;
    cout<<"1つ目 : ";
    cin >> first;
    cout<<"2つ目 : ";
    cin >> second;

    cout << first << " : " << first.length() << endl;
    cout << second << " : " << second.length() << endl;

    string combine = first + second;
    cout << combine << " : " << combine.length() << endl;
}
