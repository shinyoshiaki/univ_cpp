#ifndef ___Class_MatrixClass
#define ___Class_MatrixClass
#include <vector>
using namespace std;

#define Vector vector<double>
#define Matrix vector<vector<double>>
#define N 3

class MatrixClass
{
private:
  Matrix A;

public:
  MatrixClass(Matrix A);
  void linear_trans(Vector &x, Vector &y);
};

#endif
