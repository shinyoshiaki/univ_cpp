#include <vector>
#include "MatrixClass.h"
using namespace std;

#define Vector vector<double>
#define Matrix vector<vector<double>>
#define N 3

MatrixClass::MatrixClass(Matrix A)
{
  this->A = A;
}

void MatrixClass::linear_trans(Vector &x, Vector &y)
{
  for (int i = 0; i < N; ++i)
  {
    x[i] = 0.0;
    for (int k = 0; k < N; ++k)
      x[i] += A[i][k] * y[k];
  }
}
