#include <vector>
#include <iostream>
#include <stdio.h>
#include "VectorClass.h"
using namespace std;

#define Vector vector<double>
#define Matrix vector<vector<double>>
#define N 3

VectorClass::VectorClass(Vector v)
{
  this->v = v;
}

void VectorClass::add(Vector y, Vector z)
{
  for (int i = 0; i < N; ++i)
  {
    v[i] = y[i] + z[i];
  }
}

void VectorClass::print(string title)
{
  cout << title << endl;
  printf("   (%7.3f ", v[0]);
  for (int i = 1; i < N; ++i)
  {
    printf("%7.3f ", v[i]);
  }
  cout << ")" << endl;
}
