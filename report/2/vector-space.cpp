#include "VectorClass.h"
#include "MatrixClass.h"

int main(void)
{
    VectorClass x({1.0, 2.0, 3.0}), y({-0.1, 0.2, -0.3}),
        x_y, Ax_y, Ax, Ay, Ax_Ay;

    MatrixClass A({{1.1, -2.2, 3.3},
                   {-4.4, 5.5, -6.6},
                   {7.7, -8.8, 9.9}});

    x_y.add(x.v, y.v);
    A.linear_trans(Ax_y.v, x_y.v);
    Ax_y.print("A*(x+y) =");

    A.linear_trans(Ax.v, x.v);
    A.linear_trans(Ay.v, y.v);
    Ax_Ay.add(Ax.v, Ay.v);
    Ax_Ay.print("A*x+A*y =");
}
