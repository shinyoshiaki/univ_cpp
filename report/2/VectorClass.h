#ifndef ___Class_VectorClass
#define ___Class_VectorClass
#include <vector>
#include <iostream>
using namespace std;

#define Vector vector<double>
#define Matrix vector<vector<double>>
#define N 3

class VectorClass
{
public:
  Vector v;
  VectorClass(Vector v = {0.0, 0.0, 0.0});
  void add(Vector y, Vector z);
  void print(string title);
};
#endif
