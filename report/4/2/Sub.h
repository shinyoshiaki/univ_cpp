#ifndef ___Class_Sub
#define ___Class_Sub

#include "ABO.h"

class Sub : public ABO
{
  public:
    Sub() : ABO("-") {}
    int calc(int x, int y) { return x - y; }
};
#endif