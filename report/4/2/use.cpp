#include <iostream>
#include "ABO.h"
#include "Add.h"
#include "Sub.h"
using namespace std;

void testWith_1_2_3(ABO *ptrOp);

int main()
{
    testWith_1_2_3(new Add);
    testWith_1_2_3(new Sub);
}

void testWith_1_2_3(ABO *ptrOp)
{
    cout << "演算子" << ptrOp->toString() << "がどうか：" << endl;
    cout << "(1" << ptrOp->toString() << "2)"
         << ptrOp->toString() << "3="
         << ptrOp->calc(ptrOp->calc(1, 2), 3)
         << endl;
    cout << "1" << ptrOp->toString()
         << "(2" << ptrOp->toString() << "3)="
         << ptrOp->calc(1, ptrOp->calc(2, 3))
         << endl;
}