#ifndef ___Class_Add
#define ___Class_Add

#include "ABO.h"

class Add : public ABO
{
  public:
    Add() : ABO("+") {}
    int calc(int x, int y) { return x + y; }
};
#endif