#ifndef ___Class_ABO
#define ___Class_ABO

#include <string>

class ABO
{
    std::string symbol;

  protected:
    ABO(std::string symbol) : symbol(symbol) {}

  public:
    virtual int calc(int x, int y) = 0;
    std::string toString() { return symbol; }
};
#endif