#include <iostream>
#include "B.h"
using namespace std;

B::B(int b) : A(), b(b)
{
    cout << "   B(" << b << ") is closed." << endl;
}

B::B(int a, int b) : A(a), b(b)
{
    cout << "   B(" << a << ", " << b << ") is closed." << endl;
}

void B::func1()
{
    cout << "func1() in class B is called. a=" << a << ", b=" << b << endl;
}