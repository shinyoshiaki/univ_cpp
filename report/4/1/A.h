#ifndef ___Class_A
#define ___Class_A

class A
{
  protected:
    int a;

  public:
    A(int a = 1);
    A(double x);
    void func1();
    void func2();
};
#endif