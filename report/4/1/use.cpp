#include <iostream>
#include "A.h"
#include "B.h"
using namespace std;

int main()
{
    cout << "(1)";
    A obj;
    cout << "(2)";
    A objA(3);
    cout << "(3)";
    objA.func1();
    cout << "(4)";
    objA.func2();
    cout << "(5)";
    B objB(10, 55);
    cout << "(6)";
    objB.func1();
    cout << "(7)";
    objB.func2();
}