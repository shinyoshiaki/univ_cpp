#include <iostream>
#include "A.h"
using namespace std;

A::A(int a) : a(a)
{
    cout << "A(" << a << ") is closed." << endl;
}

A::A(double x) : a(static_cast<int>(x))
{
    cout << "A(" << x << ") is closed." << endl;
}

void A::func1()
{
    cout << "func1() in class A is called. a=" << a << endl;
}

void A::func2()
{
    cout << "func2() in class A is called a^2=" << a * a << endl;
}