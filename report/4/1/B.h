#ifndef ___Class_B
#define ___Class_B

#include "A.h"

class B : public A
{
    int b;

  public:
    B(int b);
    B(int a, int b);
    void func1();
};
#endif