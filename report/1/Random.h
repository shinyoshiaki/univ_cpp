class Random
{
private:
  unsigned long next = 1;
  int random(void);

public:
  double rand_float(void);
  int rand_int(int to);
  Random(unsigned long seed);
};

Random::Random(unsigned long seed)
{
  next = seed;
};

int Random::rand_int(int to)
{
  int k;
  k = (int)(rand_float() * to);
  return (k == to ? to - 1 : k);
};

double Random::rand_float(void)
{
  return (double)random() / 32768;
};

int Random::random(void)
{
  next = next * 1103515245 + 12345;
  return (unsigned int)((next / 65536) % 32768);
};
