#include "Random.h"
#include <iostream>
#include <iomanip>

using namespace std;
int main(void)
{
  unsigned long seed;

  cout << "Input a random seed:  ";
  cin >> seed;

  Random get_random(seed);

  for (int i = 0; i < 7; ++i)
    cout << setw(6) << get_random.rand_int(100);
  cout << endl;

  for (int i = 0; i < 5; ++i)
    cout << " " << get_random.rand_float();
  cout << endl;
}
