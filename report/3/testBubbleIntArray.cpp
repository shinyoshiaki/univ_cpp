#include <iostream>
#include "BubblesortIntArray.h"
#include "TesterForSortModuleIntArray.h"
using namespace std;

int main()
{
    TesterForSortModuleIntArray tester;
    tester.runOnRandomData(new BubblesortIntArray());
}