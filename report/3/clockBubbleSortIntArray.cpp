#include <iostream>
#include "BubblesortIntArray.h"
#include "TimerForSortModuleIntArray.h"
using namespace std;

int main()
{
    TimerForSortModuleIntArray timer;
    timer.clockingOnVariousProblems(new BubblesortIntArray());
}