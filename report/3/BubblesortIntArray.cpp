#include "BubblesortIntArray.h"

void BubblesortIntArray::sort(int a[], const int size) const
{

    for (int i = 0; i < size - 1; ++i)

        for (int j = size - 1; j > i; --j)

            if (a[j - 1] > a[j])
            {
                /* a[j-1] と a[j]
*/
                int temp = a[j - 1];
                /* の大小を調べて、 */

                a[j - 1] = a[j];
                /* 逆順なら交換する。 */

                a[j] = temp;
            }
}