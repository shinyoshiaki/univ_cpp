/* SortModuleForIntArrayモジュールの提供する                         */
/* 「int配列内の要素を昇順に並べ替える機能」が正しく動作するかどうか */
/* をテストする機能を備えたモジュールを作り出すためのクラス (仕様部) */

#ifndef ___Class_TesterForSortModuleIntArray
#define ___Class_TesterForSortModuleIntArray

#include <string>
#include "SortModuleForIntArray.h"

class TesterForSortModuleIntArray {
  static const int SIZE;   // runOnRandomData()の動作パラメータ
  static const int WIDTH;  // runOnRandomData()の動作パラメータ
public:
  // オブジェクトの説明をstringデータとして返す
  std::string toString() const
    { return "Tester for module that is to sort int data in an array"; }
  // SIZE個のランダムなデータから成る配列に対して
  // 引数で与えられた整列化モジュールを実行してみる
  void runOnRandomData(const SortModuleForIntArray* sortModule) const;
private:
  // 引数で与えられた配列の要素を順に全て出力(1行にWIDTH個ずつ)
  void prettyPrint(const int a[]) const;
};

#endif
