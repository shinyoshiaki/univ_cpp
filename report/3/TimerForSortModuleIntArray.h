/* SortModuleForIntArrayモジュールの提供する                         */
/* 「int配列内の要素を昇順に並べ替える機能」の動作速度を計測する機能 */
/* を備えたモジュールを作り出すためのクラス                 (仕様部) */

#ifndef ___Class_TimerForSortModuleIntArray
#define ___Class_TimerForSortModuleIntArray

#include <string>
#include "SortModuleForIntArray.h"

//struct Problem {
//  const int size;
//  const int iterationNum;
//};

class TimerForSortModuleIntArray {
public:
  // オブジェクトの説明をstringデータとして返す
  std::string toString() const
    { return "Timer for module that is to sort int data in an array"; }
  // 要素数が 5, 10, 25, 50, 100, 200 の場合について、
  // 引数で与えられた整列化モジュールの平均実行時間を計る
  void clockingOnVariousProblems(const SortModuleForIntArray* sortModule) const;
private:
  void setAnArrayRandom(int a[], const int size) const;
};

#endif
