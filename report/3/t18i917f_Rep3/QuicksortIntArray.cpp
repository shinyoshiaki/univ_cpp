#include "QuicksortIntArray.h"
int partition(int x[], int from, int to);
void quicksort(int x[], int from, int to);

int partition(int x[], int from, int to)
{
    int pivot;
    pivot = x[from];
    while (1)
    {
        for (; from < to && x[to] > pivot; --to)
            ;
        if (from == to)
        {
            x[from] = pivot;
            return from;
        }
        x[from++] = x[to];
        for (; from < to && x[from] <= pivot; ++from)
            ;
        if (from == to)
        {
            x[to] = pivot;
            return to;
        }
        x[to--] = x[from];
    }
}

void quicksort(int x[], int from, int to)
{
    int pivot_sub;

    if (from < to)
    {
        pivot_sub = partition(x, from, to);
        quicksort(x, from, pivot_sub - 1);
        quicksort(x, pivot_sub + 1, to);
    }
}

void QuicksortIntArray::sort(int a[], const int size) const
{
    quicksort(a, 0, size - 1);
}
