/* ストップウォッチ風に時間を測るためのオブジェクトのクラス StopWatch (仕様部) */

#ifndef ___Class_StopWatch
#define ___Class_StopWatch

#include <string>
#include <ctime>

struct ProcessTime_realTime {
  double processTime;
  double realTime;
};

class StopWatch {
  clock_t previousClock;  //前回のclock()値
  time_t  previousTime;   //前回のtime(NULL)値
  clock_t currentClock;   //現在のclock()値
  time_t  currentTime;    //現在のtime(NULL)値
public:
  StopWatch(): previousClock(-1), previousTime(-1.0),
    currentClock(-1), currentTime(-1.0) {}
  std::string toString() { return "module for measuring consumed time"; }
  //時間計測開始
  void start() ;
  //前回のマーク時点からの経過時間(プロセス時間と実時間の組,単位秒)を返す
  ProcessTime_realTime getLastIntervalTimes();
};

#endif
