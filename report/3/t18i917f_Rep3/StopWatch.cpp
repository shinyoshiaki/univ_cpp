/* ストップウォッチ風に時間を測るためのオブジェクトのクラス StopWatch (実装部) */

#include <cstddef>
#include "StopWatch.h"

// StopWatch型オブジェクト内に用意された関数群 ===============

//時間計測開始
void StopWatch::start()
{
  previousClock = clock();
  previousTime  = time(NULL);
}

//前回のマーク時点からの経過時間(プロセス時間と実時間の組,単位秒)を返す
ProcessTime_realTime StopWatch::getLastIntervalTimes()
{
  ProcessTime_realTime intervalTimes;

  currentClock = clock();
  currentTime  = time(NULL);
  intervalTimes.processTime
    = static_cast<double>(currentClock - previousClock) / CLOCKS_PER_SEC;
  intervalTimes.realTime = difftime(currentTime, previousTime);
  previousClock = currentClock;
  previousTime  = currentTime;
  return intervalTimes;
}
