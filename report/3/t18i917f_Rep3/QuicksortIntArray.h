#ifndef ___Class_QuicksortIntArray
#define ___Class_QuicksortIntArray

#include <string>
#include "SortModuleForIntArray.h"

class QuicksortIntArray : public SortModuleForIntArray
{
  public:
    // 整列化モジュールの説明 ( 主に手法 ) を答える

    std::string toString() const { return "Quicksort module"; }

    // 引数で与えられた配列内の要素を昇順に並べ替える

    void sort(int a[], const int size) const;
};

#endif
