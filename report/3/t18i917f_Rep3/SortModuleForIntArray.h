/* int配列内の要素を昇順に並べ替える機能を備えた整列化モジュール       */
/* に共通の枠組みを定める抽象基底クラス SortModuleForIntArray (仕様部) */

#ifndef ___Class_SortModuleForIntArray
#define ___Class_SortModuleForIntArray

#include <string>

class SortModuleForIntArray {
public:
  // 整列化モジュールの説明(主に手法)を答える
  virtual std::string toString() const = 0;
  // 引数で与えられた配列内の要素を昇順に並べ替える
  virtual void sort(int a[], const int size) const = 0;
};

#endif
