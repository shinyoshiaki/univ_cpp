/* SortModuleForIntArrayモジュールの提供する                         */
/* 「int配列内の要素を昇順に並べ替える機能」の動作速度を計測する機能 */
/* を備えたモジュールを作り出すためのクラス                 (実装部) */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include "StopWatch.h"
#include "TimerForSortModuleIntArray.h"
using namespace std;

// TesterForSortModuleIntArray型オブジェクト内に用意された関数群 =========

/************************************************************/
/* 要素数が 5, 10, 25, 50, 100, 200 の場合について、        */
/* 整列化プログラムの平均実行時間を計る                     */
/*------------------------------------------                */
/*   要素数が 5, 10, 25, 50, 100, 200 の場合について、それ  */
/*   ぞれ 800000回, 400000回, 160000回, 80000回, 40000回,   */
/*   20000回 次の作業を繰り返して所用時間を計り、後で1回当り */
/*   の計算時間を割り出す。                                 */
/*      ｜配列上に要素数分だけランダムに整数を生成し、      */
/*      ｜その配列要素を別途用意された整列化プログラムを    */
/*      ｜使って昇順に並べ替える。                          */
/************************************************************/
void TimerForSortModuleIntArray::
       clockingOnVariousProblems(const SortModuleForIntArray* sortModule) const
{
  const int MAX_SIZE = 200;
  const int PROB_NUM = 6;
  struct Problem {
    const int size;
    const int iterationNum;
  };
  const Problem prob[PROB_NUM] = {{5,800000}, {10,400000}, {25,160000},
                                  {50,80000}, {100,40000}, {200,20000}};
  int a[MAX_SIZE], seed;
  StopWatch timer;
  ProcessTime_realTime timeForSort[PROB_NUM], timeForInit[PROB_NUM];

  cout << "Clocking the average execution time of the program" << endl
       << "that sorts 5, 10, 25, 50, 100, or 200 elements." << endl
       << "  (*** " << sortModule->toString() << " ***)" << endl
       << "Input a random seed (0 - " << RAND_MAX << "):  ";
  cin >> seed;

  //ソート以外の部分の計算時間を測定
  srand(seed);
  for (int k=0; k<PROB_NUM; ++k) {
    timer.start();
    for (int i=0; i<prob[k].iterationNum; ++i) { /* この部分の */
      setAnArrayRandom(a, prob[k].size);         /* 計算時間を */
    }                                     /* 参考のために測る。*/
    timeForInit[k] = timer.getLastIntervalTimes();
  }

  //ソートプログラムの平均計算時間を測定
  srand(seed);
  for (int k=0; k<PROB_NUM; ++k) {
    timer.start();
    for (int i=0; i<prob[k].iterationNum; ++i) { /* この部分の */
      setAnArrayRandom(a, prob[k].size);         /* 計算時間を */
      sortModule->sort(a, prob[k].size);         /* 測る。     */
    }                                            /*            */
    timeForSort[k] = timer.getLastIntervalTimes();
                     /* 次にsort部分だけの計算時間を割り出す。 */
    timeForSort[k].processTime -= timeForInit[k].processTime;
    timeForSort[k].realTime    -= timeForInit[k].realTime;
  }

  // 測定結果の出力
  cout << endl
       << "        ** time for sort **    **time for initialize**" << endl
       << "size    process_t  real_time    process_t  real_time" << endl
       << "         (m sec)    (m sec)      (m sec)    (m sec)" << endl
       << "----    ---------  ---------    ---------  ---------" << endl;
  for (int k=0; k<PROB_NUM; ++k)
    cout << setw(4) << prob[k].size << fixed << setprecision(5)
	 << "    "
	 << setw(9) << timeForSort[k].processTime*1000.0/prob[k].iterationNum
	 << "  "
	 << setw(9) << timeForSort[k].realTime*1000.0/prob[k].iterationNum
	 << "    "
	 << setw(9) << timeForInit[k].processTime*1000.0/prob[k].iterationNum
	 << "  "
	 << setw(9) << timeForInit[k].realTime*1000.0/prob[k].iterationNum
	 << endl;
}

/*----------------------------------------------------------*/
/* 引数で与えられた配列の各要素をランダムに設定する         */
/*------------------------------------------                */
/* (仮引数) a    : int型配列                                */
/*          size : int型配列 a の大きさ                     */
/* (関数値)  :  なし                                        */
/* (機能) :  配列要素 a[0]〜a[size-1] に 0〜999 の間の乱数  */
/*           を設定する。                                   */
/*----------------------------------------------------------*/
void TimerForSortModuleIntArray::setAnArrayRandom(int a[], const int size) const
{
  for (int i=0; i<size; ++i)
    a[i] = rand() % 1000;
}
