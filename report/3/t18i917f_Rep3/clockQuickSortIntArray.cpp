#include <iostream>
#include "QuicksortIntArray.h"
#include "TimerForSortModuleIntArray.h"
using namespace std;

int main()
{
    TimerForSortModuleIntArray timer;
    timer.clockingOnVariousProblems(new QuicksortIntArray());
}