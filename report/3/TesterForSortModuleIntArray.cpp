/* SortModuleForIntArrayモジュールの提供する                         */
/* 「int配列内の要素を昇順に並べ替える機能」が正しく動作するかどうか */
/* をテストする機能を備えたモジュールを作り出すためのクラス (実装部) */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "TesterForSortModuleIntArray.h"
using namespace std;

const int TesterForSortModuleIntArray::SIZE  = 100;
const int TesterForSortModuleIntArray::WIDTH = 10;

// TesterForSortModuleIntArray型オブジェクト内に用意された関数群 =========

// SIZE個のランダムなデータから成る配列に対して
// 引数で与えられた整列化モジュールを実行してみる
void TesterForSortModuleIntArray::
       runOnRandomData(const SortModuleForIntArray* sortModule) const
{
  int a[SIZE], seed;

  //擬似乱数の設定
  cout << "擬似乱数の初期シード(int値): ";
  cin >> seed;
  if (!cin) {
    cout << "入力ミス --> 乱数シードの(再)設定なし" << endl;
  }else {
    srand(seed);
    cout << "乱数シードを" << seed << "に設定" << endl;
  }

  //配列aの各々の要素に0〜999の乱数値を設定
  for (int i=0; i<SIZE; ++i)
    a[i] = rand() % 1000;

  //整列化前の配列の内容を表示
  cout << endl << "before sorting:" << endl;
  prettyPrint(a);

  //整列化
  sortModule->sort(a, SIZE);

  //整列化後の配列の内容を表示
  cout << endl << "after sorting(" << sortModule->toString() << "):" << endl;
  prettyPrint(a);
}

// 引数で与えられた配列の要素を順に全て出力(1行にWIDTH個ずつ)
void TesterForSortModuleIntArray::prettyPrint(const int a[]) const
{
  int NumOfEleInLine=0;

  for (int i=0; i<SIZE; ++i) {
    cout << setw(7) << a[i];
    ++NumOfEleInLine;
    if (NumOfEleInLine >= WIDTH) {
      cout << endl;
      NumOfEleInLine = 0;
    }
  }
  if (NumOfEleInLine > 0)
    cout << endl;
}
