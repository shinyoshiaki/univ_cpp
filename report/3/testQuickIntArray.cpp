#include <iostream>
#include "QuicksortIntArray.h"
#include "TesterForSortModuleIntArray.h"
using namespace std;

int main()
{
    TesterForSortModuleIntArray tester;
    tester.runOnRandomData(new QuicksortIntArray());
}