#ifndef ___Class_BubblesortIntArray
#define ___Class_BubblesortIntArray

#include <string>
#include "SortModuleForIntArray.h"

class BubblesortIntArray : public SortModuleForIntArray
{
  public:
    // 整列化モジュールの説明 ( 主に手法 ) を答える

    std::string toString() const { return "Bubblesort module"; }

    // 引数で与えられた配列内の要素を昇順に並べ替える

    void sort(int a[], const int size) const;
};

#endif